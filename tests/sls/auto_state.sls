test-autostate-create:
  tests.auto.present:
    - resource_name: foo
    - name: foo
    - param: 1

test-autostate-present:
  tests.auto.present:
    - resource_name: foo
    - name: foo
    - param: 1
    - resource_id: foo

test-autostate-update:
  tests.auto.present:
    - resource_name: foo
    - name: foo
    - resource_id: foo
    - param: 2

test-autostate-delete:
  tests.auto.absent:
    - resource_name: foo
    - resource_id: foo
    - name: foo

test-autostate-absent:
  tests.auto.absent:
    - resource_name: foo
    - name: foo
