async def test_acct_blob(hub):
    """
    Verify that an acct_blob can be passed to the idem.acct.ctx function
    """
    fernet_plugin = hub.crypto.fernet
    acct_key = fernet_plugin.generate_key()
    assert acct_key

    plaintext = {"test": {"default": {"asdf": "jkl;"}}}

    acct_blob = fernet_plugin.encrypt(data=plaintext, key=acct_key)
    ctx = await hub.idem.acct.ctx(
        path="states.tst_ctx", profile="default", acct_blob=acct_blob, acct_key=acct_key
    )

    assert ctx.acct == {"asdf": "jkl;"}
