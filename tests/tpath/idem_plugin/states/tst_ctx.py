def __init__(hub):
    hub.states.tst_ctx.ACCT = ["test"]


def acct(hub, ctx, name):
    """
    Return exactly what was passed into ctx.acct
    """
    ret = {
        "name": name,
        "result": True,
        "comment": None,
        "old_state": None,
        "new_state": ctx.acct,
    }
    if not ctx.acct:
        ret["result"] = False

    return ret
