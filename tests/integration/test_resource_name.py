import pathlib

import msgpack
import pytest_idem.runner as runner
from pytest_idem.runner import idem_cli


def test_resource_name(tmpdir, tests_dir):
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()
    create_resource_in_esm = """
    test_resource:
        test_resource.present:
          - resource_name: test_resource_name
          - name: test_name
          - changes: {}
          - new_state: {resource_id: resource123}
          - skip_esm: False
    """
    ret = execute_sls(cache_dir, create_resource_in_esm)
    assert "test_resource_|-test_resource_|-test_resource_name_|-present" in ret

    assert esm_cache.exists()
    # resource_id is stored in ESM
    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        assert data["test_resource_|-test_resource_|-test_resource_name_|-"] == {
            "resource_id": "resource123"
        }


def test_rename_resource(tmpdir, tests_dir):
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()
    create_resource_in_esm = """
    test_resource:
        test_resource.present:
          - name: test_1
          - changes: {}
          - new_state: {resource_id: resource123}
          - skip_esm: False
    """

    # Create resource in ESM
    execute_sls(cache_dir, create_resource_in_esm)

    assert esm_cache.exists()
    # resource_id is stored in ESM
    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        assert data["test_resource_|-test_resource_|-test_resource_|-"] == {
            "resource_id": "resource123"
        }

    # Execute the same resource, but with new name
    rename_resource = """
    test_resource:
        test_resource.present:
          - name: test_2
          - skip_esm: False
    """
    output = execute_sls(cache_dir, rename_resource)

    assert 1 == len(output), len(output)
    output["test_resource_|-test_resource_|-test_resource_|-present"]["old_state"] == {
        "resource_id": "resource123"
    }


def test_resource_names(tmpdir, tests_dir):
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()
    create_resource_in_esm = """
    test_resource:
        test_resource.present:
          - names:
            - test_name_1
            - test_name_2
          - changes: {}
          - new_state: {resource_id: resource123}
          - skip_esm: False
    """
    ret = execute_sls(cache_dir, create_resource_in_esm)
    assert "test_resource_|-test_resource_|-test_name_1_|-present" in ret
    assert "test_resource_|-test_resource_|-test_name_2_|-present" in ret

    assert esm_cache.exists()
    # resource_id is stored in ESM
    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        assert data["test_resource_|-test_resource_|-test_name_1_|-"] == {
            "resource_id": "resource123"
        }
        assert data["test_resource_|-test_resource_|-test_name_2_|-"] == {
            "resource_id": "resource123"
        }


def test_get_resource_name_from_esm(tmpdir, tests_dir):
    # When the resource name is removed from SLS but exists in ESM
    # read it from ESM. (Similar to resource_id)
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()
    create_resource_in_esm = """
    test_resource:
        test_resource.present:
          - changes: {}
          - new_state: {resource_id: resource123, name: name_from_esm}
          - skip_esm: False
    """
    ret = execute_sls(cache_dir, create_resource_in_esm)
    assert "test_resource_|-test_resource_|-test_resource_|-present" in ret

    assert esm_cache.exists()
    # resource_id is stored in ESM
    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        resource = data["test_resource_|-test_resource_|-test_resource_|-"]
        assert resource is not None
        assert resource["resource_id"] == "resource123"
        assert resource["name"] == "name_from_esm"

    # Update the resource but remove 'name', it will be read from ESM
    update_resource_in_esm = """
    test_resource:
        test_resource.present:
          - changes: {}
          - new_state: {resource_id: resource123, key: value}
          - skip_esm: False
    """
    ret = execute_sls(cache_dir, update_resource_in_esm)
    assert "test_resource_|-test_resource_|-test_resource_|-present" in ret
    assert (
        ret["test_resource_|-test_resource_|-test_resource_|-present"].get("name")
        == "name_from_esm"
    )


def execute_sls(cache_dir, sls_to_execute):
    acct_data = {"profiles": {"test": {"default": {}}}}
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(sls_to_execute)

        return idem_cli(
            "state",
            fh,
            f"--cache-dir={cache_dir / 'cache'}",
            f"--root-dir={cache_dir}",
            "--esm-plugin=local",
            "--run-name=test",
            check=True,
            acct_data=acct_data,
        ).json
