from pytest_idem.runner import run_sls


def test_render_empty(hub, mock_hub):
    """Test that Idem return empty result when sls is empty after rendering"""
    empty_sls = "empty"
    hub.log.info = mock_hub.log.info

    # This empty file is not empty content. But after Jinjia rendering, the rendered result is None
    ret = run_sls([empty_sls], hub=hub)

    assert len(ret) == 0
    mock_hub.log.info.assert_called_with(
        f"SLS ref '{empty_sls}' is not resolved to any state."
    )


def test_render_empty_and_valid(idem_cli, tests_dir):
    """Test that Idem returns a empty result if sls is empty after rendering, and continues to process other sls files"""

    ret = idem_cli(
        "state",
        tests_dir / "sls" / "empty.sls",
        tests_dir / "sls" / "simple.sls",
        "--log-level=info",
    )

    assert "idem.tests.sls.empty' is not resolved to any state." in ret.stderr
    assert ret.json["test_|-happy_|-happy_|-nop"]["result"] is True
    assert ret.json["test_|-sad_|-sad_|-fail_without_changes"]["result"] is False
