import pytest_idem.runner as runner
from pytest_idem.runner import idem_cli


RESOURCE_SLS = """
test_resource:
    test_resource.present:
      - resource_name: test_changes_1
      - changes: {}
      - old_state: {some_property: value}
      - new_state: {some_property: value}
"""


def test_changes_not_being_recalculated(tests_dir):
    output = execute_sls(RESOURCE_SLS)

    assert 1 == len(output), len(output)
    tag = "test_resource_|-test_resource_|-test_changes_1_|-present"

    result = output[tag]

    # Result is True
    assert result["result"] is True

    # Check that changes are as set, not recalculated
    assert result["changes"] == {}


def execute_sls(sls_to_execute):
    acct_data = {"profiles": {"test": {"default": {}}}}
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(sls_to_execute)

        return idem_cli(
            "state",
            fh,
            "--run-name=test",
            check=True,
            acct_data=acct_data,
        ).json
