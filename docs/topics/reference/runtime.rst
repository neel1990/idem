=======
Runtime
=======

Runtime is an execution runtime to execute states.

The ``runtime`` argument can be used in any ``idem state`` subcommand as shown in below example:

.. code-block:: bash

    $ idem state test.sls --runtime=execution_runtime

Idem supports following execution_runtime: 'parallel' (default), and 'serial'

Parallel
========

This is a default execution runtime. It executes 50 states (default) in parallel at a time.

The default batch size can be configured with ``batch-size`` argument.

The batch size with a negative value will run all states in parallel.

.. code-block:: bash

    $ idem state test.sls
    $ idem state test.sls --runtime=parallel
    $ idem state test.sls --runtime=parallel --batch-size=N

Serial
======

With ``serial`` runtime states are executed one at a time.

.. code-block:: bash

    $ idem state test.sls --runtime=serial
