======================
Write To File Function
======================

Idem supports writing any data to a file.
The 'parameters' might include argument bindings to record details from
resources referenced by the same Idem invocation.
The function can accept a Jinja template or a template file, which can
generate a script from the 'parameters'.
If you omit a template, and the 'parameters' are a dictionary, they will
be written to the file in a json format.


For example:

.. code-block:: yaml

    my-output-file:
        data.write:
        - file_name: "/files/audit.log"
        - template: '{% raw  %}
                       {% for resource_name, resource_id in parameters.items() %}
                            Created {{ resource_name }}  {{ resource_id }}
                       {% endfor %}
                    {%  endraw %}'
       - parameters:
            ${resource-1:name}: ${resource-1:id}
            ${resource-2:name}: ${resource-2:id}

The preceding write function produces a ``/files/audit.log`` text file that contains the following:

.. code-block:: text

    Created subnet-1 a7dd1f64-8e02-11ec-b909-0242ac120002
    Created subnet-2 b08bcf20-8e02-11ec-b909-0242ac120002
