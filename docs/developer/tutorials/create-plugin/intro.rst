==========================
Working with Idem plug-ins
==========================

.. warning::
   This document is a work in progress.

To work with your environment, Idem needs a dedicated account plug-in. You can
create a plug-in by hand or by using a parser.

About POP and plug-ins
----------------------

Idem is a modular system that follows the plug-in oriented programming (POP)
open source model. For Idem to run against your specific cloud environment, you
first need to create an account plug-in. POP includes a pop-create tool for
setting up new plug-in projects.


About Idem cloud
----------------

For Idem to interface with your environment on cloud hosts such as AWS or
Azure, you need a dedicated plug-in. The pop-create-idem tool is an extension
of pop-create that creates boilerplate code for new Idem cloud projects. New
projects are based on the idem-cloud open source project of methods common to
all Idem cloud implementations.

The pop-create-idem repo is found here: https://gitlab.com/vmware/idem/pop-create-idem
