======================
Transparent Requisites
======================

Transparent requisites allow requisites to be defined on a function by function basis. This
means that a given function can always requires any instance of
another function, in the background. This makes it easy for
state authors to ensure that executions are always executed in the
correct order without the end user needing to define those orders.

It is easy to do, at the top of your system module just define the
``TREQ`` dict, this dict defines what functions will require what
other functions:

.. code-block:: python

    TREQ = {
        "present": {
            "require": [
                "test.other.present",
            ]
        },
    }

This stanza will look for the function named ``present`` inside of the module
that it is defined in, then it will add ``require : - test.other.present`` for every
instance found of ``test.other.present`` in the current run.
If ``test.other.present`` is never used, then no requisites are set.
Any requisite can be used, and multiple requisites can be used.

Transparent Requisite In Contracts
----------------------------------

Transparent requisites can be added to Idem contracts. All the states
that implement contract automatically inherit transparent requisite defined
in the contract.

Unique Transparent Requisite
============================

Another type of transparent requisite is ``unique``.
A function can be declared ``unique`` to prevent concurrent executions.
The ``unique`` transparent requisite is significant in case of a parallel execution (default).
``TREQ`` dict at the top of the system module define ``unique`` with a list of functions within the module.

.. code-block:: python

    TREQ = {
        "unique": [
            "test.create",
            "test.delete",
        ]
    }

In the example above, the instances of ``test.create`` within the current run will be invoked serially, and all the instances of ``test.delete`` will be invoked serially.
Instance of ``test.create`` and ``test.delete`` can be invoked in parallel.
It is achieved by selecting a single instance of the unique function, and setting the other instances of the same function as dependent on it.
During the next run, a new instance will be selected.
The ``unique`` requisite is re-evaluated in each run.
